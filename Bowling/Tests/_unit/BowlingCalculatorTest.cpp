#include <BowlingCalculator/BowlingCalculator.h>
#include <gmock/gmock.h>

using namespace testing;
using namespace Bowling;

class BowlingCalculatorTest : public Test
{
public:
    BowlingCalculator Calculator;

    void RollMany(const int rolls, const int pins)
    {
        for(int i = 0; i < rolls; i++)
        {
            Calculator.Roll(pins);
        }
    }

    void RollSpare()
    {
        Calculator.Roll(5);
        Calculator.Roll(5);
    }

    void RollStrike()
    {
        Calculator.Roll(10);
    }
};

TEST_F(BowlingCalculatorTest, GivenGutterGame_WhenCalculatingScore_ThenScoreIsZero)
{
    RollMany(20, 0);
    EXPECT_THAT(Calculator.Score(), Eq(0));
}

TEST_F(BowlingCalculatorTest, GivenAllOnes_WhenCalculatingScore_ThenScoreIsTwenty)
{
    RollMany(20, 1);
    EXPECT_THAT(Calculator.Score(), Eq(20));
}

TEST_F(BowlingCalculatorTest, GivenSpare_WhenCalculatingScore_ThenScoreIsSixteen)
{
    RollSpare();
    Calculator.Roll(3);
    RollMany(17, 0);
    EXPECT_THAT(Calculator.Score(), Eq(16));
}

TEST_F(BowlingCalculatorTest, GivenStrike_WhenCalculatingScore_ThenScoreIsTwentyFour)
{
    RollStrike();
    Calculator.Roll(3);
    Calculator.Roll(4);
    RollMany(16, 0);
    EXPECT_THAT(Calculator.Score(), Eq(24));
}

TEST_F(BowlingCalculatorTest, GivenPerfectGame_WhenCalculatingScore_ThenScoreIsThreeHundred)
{
    RollMany(12, 10);
    EXPECT_THAT(Calculator.Score(), Eq(300));
}

TEST_F(BowlingCalculatorTest, GivenAllSpares_WhenCalculatingScore_ThenScoreIsOneHundredFifty)
{
    RollMany(21, 5);
    EXPECT_THAT(Calculator.Score(), Eq(150));
}

TEST_F(BowlingCalculatorTest, Given10Spares_WhenCalculatingScore_ThenScoreIs60)
{
    RollMany(10, 5);
    EXPECT_THAT(Calculator.Score(), Eq(60));
}

TEST_F(BowlingCalculatorTest, Given3Strikes_WhenCalculatingScore_ThenScoreIs30)
{
    RollMany(3, 10);
    EXPECT_THAT(Calculator.Score(), Eq(30));
}

TEST_F(BowlingCalculatorTest, Given3StrikesFollowedBy4Pins_WhenCalculatingScore_ThenScoreIs54)
{
    RollMany(3, 10);
    Calculator.Roll(4);
    EXPECT_THAT(Calculator.Score(), Eq(54));
}

TEST_F(BowlingCalculatorTest, Given3StrikesFollowedBy4PinsThen2Pins_WhenCalculatingScore_ThenScoreIs76)
{
    RollMany(3, 10);
    Calculator.Roll(4);
    Calculator.Roll(2);
    EXPECT_THAT(Calculator.Score(), Eq(76));
}

TEST_F(BowlingCalculatorTest, GivenPartialGameWhenNewGameThenScoreIsZero)
{
    RollMany(3, 10);
    Calculator.NewGame();
    EXPECT_THAT(Calculator.Score(), Eq(0));
}
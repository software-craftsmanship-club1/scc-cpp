#include "BowlingCalculator.h"

namespace
{
    constexpr int FullFrameScore = 10;
}
namespace Bowling
{
    BowlingCalculator::BowlingCalculator()
        : m_rollList()
    {
    }

    BowlingCalculator::~BowlingCalculator()
    {
    }

    void BowlingCalculator::Roll(const int pins)
    {
        m_rollList.push_back(pins);
    }

    int BowlingCalculator::Score() const
    {
        constexpr unsigned int maxFrames = 10;
        int score = 0;
        int frameIndex = 0;
        for (int frame = 0; frame < maxFrames; frame++)
        {
            try
            {
                if(IsStrike(frameIndex))
                {
                    score += FullFrameScore + StrikeBonus(frameIndex);
                    frameIndex++;
                }
                else if(IsSpare(frameIndex))
                {
                    score += FullFrameScore + SpareBonus(frameIndex);
                    frameIndex += 2;
                }
                else
                {
                    score += SumBallsInFrame(frameIndex);
                    frameIndex += 2;
                }
            }
            catch(const std::exception& e)
            {
                return score;
            }
        }
        return score;
    }

    bool BowlingCalculator::IsStrike(const int frameIndex) const
    {
        return m_rollList.at(frameIndex) == FullFrameScore;
    }

    int BowlingCalculator::StrikeBonus(const int frameIndex) const
    {
        return m_rollList.at(frameIndex + 1) + m_rollList.at(frameIndex + 2);
    }

    bool BowlingCalculator::IsSpare(const int frameIndex) const
    {
        return m_rollList.at(frameIndex) + m_rollList.at(frameIndex + 1) == FullFrameScore;
    }

    int BowlingCalculator::SpareBonus(const int frameIndex) const
    {
        return m_rollList.at(frameIndex + 2);
    }

    int BowlingCalculator::SumBallsInFrame(const int frameIndex) const
    {
        return m_rollList.at(frameIndex) + m_rollList.at(frameIndex + 1);
    }

    void BowlingCalculator::NewGame()
    {
        m_rollList.clear();
    }
}
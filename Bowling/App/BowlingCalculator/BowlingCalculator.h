#include <vector>

namespace Bowling
{
    class BowlingCalculator
    {
    public:
        BowlingCalculator();
        ~BowlingCalculator();
        void Roll(const int pins);
        int Score() const;
        void NewGame();

    private:
        bool IsStrike(const int frameIndex) const;
        bool IsSpare(const int frameIndex) const;
        int StrikeBonus(const int frameIndex) const;
        int SpareBonus(const int frameIndex) const;
        int SumBallsInFrame(const int frameIndex) const;

        std::vector<int> m_rollList;
    };
}
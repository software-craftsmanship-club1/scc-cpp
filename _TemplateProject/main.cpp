#include <BowlingCalculator/BowlingCalculator.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
// TODO: Create string parser class
void ScoreFrame(Bowling::BowlingCalculator& calculator, const std::string& frame)
{
    if("X" == frame)
    {
        calculator.Roll(10);
    }
    else
    {
        int firstRollValue = 0;
        std::string firstRoll = frame.substr(0, 1);
        if("-" == firstRoll)
        {
            calculator.Roll(0);
        }
        else
        {
            firstRollValue = std::stoi(firstRoll);
            calculator.Roll(firstRollValue);
        }

        std::string secondRoll = frame.substr(1, 1);
        if("-" == secondRoll)
        {
            calculator.Roll(0);
        }
        else if("/" == secondRoll)
        {
            calculator.Roll(10 - firstRollValue);
        }
        else
        {
            calculator.Roll(std::stoi(secondRoll));
        }

        if(frame.length() == 3)
        {
            std::string thirdRoll = frame.substr(2, 1);
            if("-" == thirdRoll)
            {
                calculator.Roll(0);
            }
            else
            {
                calculator.Roll(std::stoi(thirdRoll));
            }
        }
    }
}

void ParseGameString(Bowling::BowlingCalculator& calculator, std::string scoreString)
{
    std::vector<std::string> frames;
    std::istringstream iss(scoreString);
    std::string frame;
    while (std::getline(iss, frame, ' '))
    {
        frames.push_back(frame);
    }
    for(const std::string& frame : frames)
    {
        ScoreFrame(calculator, frame);
    }
}

int main()
{
    std::string testGame1 = "X X X X X X X X X X X X";
    std::string testGame2 = "5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5";
    std::string testGame3 = "9- 9- 9- 9- 9- 9- 9- 9- 9- 9-";
    std::string testGame4 = "3/ 5/ 52 21 X X 11 5/ 8/ 5/-";
    std::string testGame5 = "3/ -- 52 21 X 24 11 -- -3 -/3";

    // Split testGame1 on spaces
    Bowling::BowlingCalculator scoreCalculator;
    ParseGameString(scoreCalculator, testGame1);
    std::cout << "Score for testGame1: " << scoreCalculator.Score() << std::endl;

    scoreCalculator.NewGame();
    ParseGameString(scoreCalculator, testGame2);
    std::cout << "Score for testGame2: " << scoreCalculator.Score() << std::endl;

    scoreCalculator.NewGame();
    ParseGameString(scoreCalculator, testGame3);
    std::cout << "Score for testGame3: " << scoreCalculator.Score() << std::endl;

    scoreCalculator.NewGame();
    ParseGameString(scoreCalculator, testGame4);
    std::cout << "Score for testGame4: " << scoreCalculator.Score() << std::endl;

    scoreCalculator.NewGame();
    ParseGameString(scoreCalculator, testGame5);
    std::cout << "Score for testGame5: " << scoreCalculator.Score() << std::endl;

    return 0;
}